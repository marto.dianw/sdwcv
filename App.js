import React, {Component} from 'react';
import {
  View,
  SafeAreaView,
} from 'react-native';
import LinkedRouter from './src/LinkedRouter';

export default class App extends Component<Props> {
  render() {
    return (
      <SafeAreaView style={{flex: 1}}>
        <View style={{flex:1}}>
            <LinkedRouter />
        </View>
      </SafeAreaView>
    );
  }
}
