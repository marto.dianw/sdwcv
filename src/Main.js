import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text
} from 'react-native';


class Main extends Component<{}> {

	render() {
        return(
            <View style={styles.container}>
                <Text>Hello</Text>
            </View>
        )
	}
}

const styles = StyleSheet.create({
  container : {
    flex: 1
  }
});

export default Main
