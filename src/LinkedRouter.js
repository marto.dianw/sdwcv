import React, { Component } from 'react';
import {Router, Stack, Scene} from 'react-native-router-flux';
import Main from './Main';


export default class LinkedRouter extends Component<{}> {
	render() {
		return(
			<Router>
                <Scene key="root">
                    <Scene 
                        key="main" 
                        component={Main} 
                        initial={true} 
                        title='Welcome'
                    />
                </Scene>
			 </Router>
			)
	}
}
